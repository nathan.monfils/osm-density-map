import harversine
import osmobjects

class Border:
    def __init__(self, border_ways):
        self.border_ways = border_ways

        self.json = {
            'lines': []
        }

        self.min_lat = 90.0
        self.min_lon = 180.0
        self.max_lat = -90.0
        self.max_lon = -180.0

        for way in border_ways:
            for node in way.nodes:
                if node[0] < self.min_lat:
                    self.min_lat = node[0]
                if node[0] > self.max_lat:
                    self.max_lat = node[0]

                if node[1] < self.min_lon:
                    self.min_lon = node[1]
                if node[1] > self.max_lon:
                    self.max_lon = node[1]

        self.json['min'] = {
            'lat': self.min_lat,
            'lon': self.min_lon
        }
        self.json['max'] = {
            'lat': self.max_lat,
            'lon': self.max_lon
        }

        dlat = self.max_lat - self.min_lat
        dlon = self.max_lon - self.min_lon

        X_RES = 64
        Y_RES = 64

        grab_radius = harversine.harversine((self.min_lat, self.min_lon), (self.min_lat + dlat / float(Y_RES), self.min_lon + dlon / float(X_RES))) / 2.0

        border_nodes = []
        for way in border_ways:
            for node in way.nodes:
                border_nodes.append(node)

        self.border_nodes_simplified = []
        for x in range(0, X_RES):
            for y in range(0, Y_RES):
                point = (self.min_lat + y * dlat/float(Y_RES), self.min_lon + x * dlon/float(X_RES))
                for node in border_nodes:
                    if harversine.harversine(node, point) < grab_radius:
                        self.border_nodes_simplified.append((x / float(X_RES), y / float(Y_RES)))
                        break

        del self.border_ways

        self.json = {
            'min': {
                'lat': self.min_lat,
                'lon': self.min_lon
            },
            'max': {
                'lat': self.max_lat,
                'lon': self.max_lon
            },
            'points': []
        }

        for node in self.border_nodes_simplified:
            self.json['points'].append({
                'x': node[0],
                'y': 1.0 - node[1]
            })
