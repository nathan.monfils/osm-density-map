#https://andrew.hedges.name/experiments/haversine/

from math import radians, sin, cos, atan2, sqrt

# a = (lat, lon), b = (lat, lon) (in degrees)
def harversine(a, b):
    # Earth radius (km)
    R = 6373.0

    # Convert to radians
    a = (radians(a[0]), radians(a[1]))
    b = (radians(b[0]), radians(b[1]))

    dlat = a[0] - b[0]
    dlon = a[1] - b[1]

    a = (sin(dlat/2))**2 + cos(a[0]) * cos(b[0]) * (sin(dlon/2))**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))

    return R*c
