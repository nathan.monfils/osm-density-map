# PyOsmium's ways cause a SEGFAULT when being access from a dictionnary, so we store them in a custom, pythonic struct instead
class MyWay():
    def __init__(self, way):
        self.id = way.id
        self.nodes = []
        for node in way.nodes:
            self.nodes.append((node.location.lat, node.location.lon))
