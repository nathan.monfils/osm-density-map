#!/usr/bin/env python3

import os
import sys
import json
import pickle
from flask import Flask, Response, request

import osm

app = Flask('belgium-density-backend')
load_border_from_cache = os.path.isfile('.cache.border_' + sys.argv[2])
load_densities_from_cache = os.path.isfile('.cache.densities_' + sys.argv[2])
osmRegion = osm.OSMRegion(sys.argv[2], not load_border_from_cache)

border = None
densities = None

@app.route('/density')
def serve_density():
    response = None
    if densities != None:
        if 'type' in request.args:
            if request.args['type'] == 'roundabouts':
                j = json.dumps(densities['roundabouts'])
            elif request.args['type'] == 'buildings':
                j = json.dumps(densities['buildings'])
            elif request.args['type'] == 'hospitals':
                j = json.dumps(densities['hospitals'])
            elif request.args['type'] == 'waterways':
                j = json.dumps(densities['waterways'])
            else:
                print('Error: Unknwon density type.')
                j = json.dumps({
                    'Error': 'Unknown density type.'
                })
        else:
            print('Error: Did not find type arg.')
            j = json.dumps({
                'Error': 'Did not find type arg.'
            })
    else:
        j = json.dumps({
            'Error': 'Density is None. This should not happen and is a backend error.'
        })
    response = Response(j)
    response.mimetype = 'application/json'
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/border')
def serve_border():
    response = None
    if border != None:
        response = Response(json.dumps(border.json))
    else:
        response = Response(json.dumps({
            'Error': 'Border is None. This should not happen and is a backend error.'
        }))
    response.mimetype = 'application/json'
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: ./backend.py <.osm.pbf location> <ISO 3166-1 country tag>')
        exit(1)

    if not load_border_from_cache or not load_densities_from_cache:
        print('Parsing .osm.pbf file...')
        osmRegion.apply_file(sys.argv[1], locations=True, idx='sparse_mem_array')

    if load_border_from_cache:
        print('Loading border from cache...')
        with open('.cache.border_' + sys.argv[2], 'rb') as f:
            border = pickle.load(f)
        osmRegion.border = border
    else:
        print('Drawing border...')
        border = osmRegion.draw_border()
        print('Saving border to cache...')
        with open('.cache.border_' + sys.argv[2], 'wb') as f:
            pickle.dump(border, f, protocol=pickle.HIGHEST_PROTOCOL)

    if load_densities_from_cache:
        print('Loading densities from cache...')
        with open('.cache.densities_' + sys.argv[2], 'rb') as f:
            densities = pickle.load(f)
    else:
        print('Drawing densities...')
        densities = osmRegion.draw_densities()
        print('Saving densities to cache...')
        with open('.cache.densities_' + sys.argv[2], 'wb') as f:
            pickle.dump(densities, f, protocol=pickle.HIGHEST_PROTOCOL)

    app.run()
