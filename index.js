var width = 1.0;
var height = 1.0;

function getProjection(border) {
    let min_lat = border.min.lat;
    let max_lat = border.max.lat;
    let min_lon = border.min.lon;
    let max_lon = border.max.lon;
    let deltax = (new LatLon(min_lat, min_lon)).distanceTo((new LatLon(min_lat, max_lon)));
    let deltay = (new LatLon(min_lat, min_lon)).distanceTo((new LatLon(max_lat, min_lon)));

    // Flat projection (no fancy tricks, we are rendering a country not a continent)
    let projection_x = (deltay / deltax) > 1 ? (deltax / deltay) : 1.0;
    let projection_y = (deltax / deltay) > 1 ? (deltay / deltax) : 1.0;

    return [projection_x, projection_y]
}

function drawBorder(map, border) {
    for (point of border.points) {
        c = map.circle(3.0);
        c.center(point.x * width, point.y * height);
        c.fill('#EEE');
    }
}

function drawDensity(map, density) {
    let max_count = 0
    for (point of density.points) {
        if (point.count > max_count) {
            max_count = point.count
        }
    }

    for (point of density.points) {
        if (point.count > 0) {
            let saturation = point.count / max_count;

            c = map.circle(5.0);
            c.center(point.x * width, point.y * height);
            c.fill('rgba(255, 0, 0, ' + saturation + ')');
        }
    }
}

let url = new URL(window.location.href);
let density_type = url.searchParams.get('type');

let req = new XMLHttpRequest();
req.open('GET', 'http://127.0.0.1:5000/border', true)
req.send()
req.onreadystatechange = function() {
    if (this.status == 200 && this.readyState == 4) {
        projection = getProjection(JSON.parse(this.response))

        // We can use the projection right here thanks to fancy SVG, rather than manually compute it for each point!
        console.log(projection)
        width = projection[0] * 1000.0;
        height = projection[1] * 1000.0;

        let map = SVG('map').size(width, height);

        // We create the SVG *after* we have a projection so that we can set the viewport appropriately
        map.viewbox(0, 0, width, height);

        let background = map.rect(width, height).fill('#000');

        drawBorder(map, JSON.parse(this.response));

        let req = new XMLHttpRequest();
        req.open('GET', 'http://127.0.0.1:5000/density?type=' + density_type, true)
        req.send()
        req.onreadystatechange = function() {
            if (this.status == 200 && this.readyState == 4) {
                drawDensity(map, JSON.parse(this.response));
            }
        }
    }
}

