OpenStreetMap density map
=========================

![Example output](output_example.png)

This experiment shows a SVG heatmap for several OSM entity types (roundabouts, buildings, hospitals and waterways). It is easily expandable to any other entity type.

The backend makes use of [PyOsmium](https://osmcode.org/pyosmium/)+[shapely](https://pypi.org/project/Shapely/) to parse *.osm.pbf files and [Flask](http://flask.pocoo.org/) to serve the files.
The frontend makes use of [svg.js](http://svgjs.com/) for rendering and [geodesy](http://www.movable-type.co.uk/scripts/latlong.html) for distance calculations.

Usage
-----

Install PyOsmium, Shapely and Flask through your favourite python 3 package manager. Download the country of your choice from [geofabrik](https://download.geofabrik.de/) (or any of your favourite OpenStreetMap providers). Keep in mind that the script doesn't diffentiate from entities outside and inside country borders (as it makes the code *significantly* more complicated), so you probably don't want to use the whole Planet.osm file.

Run the backend `cd backend && ./backend.py /path/to/country.osm.pbf [ISO 3166-1 country code]`, then access the `index.html` page in your favourite browser.
